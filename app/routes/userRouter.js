const {middleWare} = require("../middleware/middleware")
const {ArrayOfStaffs} = require("../../data")
const express = require("express")
var req = express.request
const userRouter = express.Router()
userRouter.get("/",middleWare,(req,res)=>{
    let filterAge = parseInt(req.query.age)
    if (Number.isInteger(filterAge)){
        let resultStaffs = ArrayOfStaffs.filter(data => {
            return data.age>filterAge
        })
       
        return res.status(201).json(resultStaffs)
    }
    else{
        return res.status(201).json(ArrayOfStaffs)
    }
})
userRouter.get("/:userid",middleWare,(req,res)=>{
    let filterId = parseInt(req.params.userid)
    if (Number.isInteger(filterId)){
        let resultStaffs = ArrayOfStaffs.filter(data => {
            return data.id==filterId
        })
       
        return res.status(201).json(resultStaffs[0])
    }
    else return res.status(400).json("bad request")
})

module.exports = {userRouter}