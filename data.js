class Staff {
    constructor( paramId,paramName,paramPosition,paramOffice,paramAge,paramStartDate){
        this.id = paramId;
        this.name = paramName;
        this.position = paramPosition;
        this.office = paramOffice;
        this.age = paramAge;
        this.startDate = paramStartDate;
    }
}
var ArrayOfStaffs = [
    new Staff("1","Airi Satou","Accountant","Tokyo","33","2008/11/28"),
    new Staff("2","Angelica Ramos","Chief Executive Officer (CEO)","London","47","2009/10/09"),
    new Staff("3","Ashton Cox","Junior Technical Author","San Francisco","66","2009/01/12"),
    new Staff("4","Bradley Greer","Software Engineer","London","41","2012/10/13"),
    new Staff("5","Brenden Wagner","Software Engineer","San Francisco","28","2011/06/07"),
    new Staff("6","Brielle Williamson","Integration Specialist","New York","61","2012/12/02"),
    new Staff("7","Bruno Nash","Software Engineer","London","38","2011/05/03"),
    new Staff("8","Caesar Vance","Pre-Sales Support","New York","21","2011/12/12"),
    new Staff("9","Cara Stevens","Sales Assistant","New York","46","2011/12/06"),
    new Staff("10","Cedric Kelly","Senior Javascript Developer","Edinburgh","22","2012/03/29")
]

module.exports = {ArrayOfStaffs}