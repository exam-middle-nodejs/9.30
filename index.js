const express = require("express")
const app = express()
const port = 8000;
const {userRouter} = require("./app/routes/userRouter")
app.use("/users",userRouter)

app.listen(port,()=>{
    console.log("app listening")
})